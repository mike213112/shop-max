import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, FormGroup, FormControlName } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { LoginComponent } from './componentes/user/login/login.component';
import { RegisterComponent } from './componentes/user/register/register.component';

import { PersonService } from './services/persona/person.service';
import { ProductService } from './services/producto/product.service';
import { TiendaComponent } from './componentes/principal/tienda/tienda.component';
import { CatalogoComponent } from './componentes/principal/catalogo/catalogo.component';
import { NuevosProductosComponent } from './componentes/principal/nuevos-productos/nuevos-productos.component';
import { ContactoComponent } from './componentes/principal/contacto/contacto.component';
import { CarritoComponent } from './componentes/principal/carrito/carrito.component';
import { Navegacion1Component } from './componentes/navbar/navegacion1/navegacion1.component';
import { Navegacion2Component } from './componentes/navbar/navegacion2/navegacion2.component';
import { NuevosComponent } from './componentes/productos/nuevos/nuevos.component';
import { ListComponent } from './componentes/productos/list/list.component';
import { EditComponent } from './componentes/productos/edit/edit.component';
import { FooternavbarComponent } from './componentes/navbar/footernavbar/footernavbar.component';
import { AbonosComponent } from './componentes/categorias/abonos/abonos.component';
import { ArbolesComponent } from './componentes/categorias/arboles/arboles.component';
import { ArbustosComponent } from './componentes/categorias/arbustos/arbustos.component';
import { AreasVerdesComponent } from './componentes/categorias/areas-verdes/areas-verdes.component';
import { CespedesComponent } from './componentes/categorias/cespedes/cespedes.component';
import { DecoracionComponent } from './componentes/categorias/decoracion/decoracion.component';
import { FloresComponent } from './componentes/categorias/flores/flores.component';
import { HerramientasComponent } from './componentes/categorias/herramientas/herramientas.component';
import { PiedrasComponent } from './componentes/categorias/piedras/piedras.component';
import { TierraComponent } from './componentes/categorias/tierra/tierra.component';
import { AbonosnuevoComponent } from './componentes/productos/abonosnuevo/abonosnuevo.component';
import { ArbolesnuevoComponent } from './componentes/productos/arbolesnuevo/arbolesnuevo.component';
import { ArbustosnuevoComponent } from './componentes/productos/arbustosnuevo/arbustosnuevo.component';
import { AreasVerdesnuevoComponent } from './componentes/productos/areas-verdesnuevo/areas-verdesnuevo.component';
import { CespedesnuevoComponent } from './componentes/productos/cespedesnuevo/cespedesnuevo.component';
import { DecoracionnuevoComponent } from './componentes/productos/decoracionnuevo/decoracionnuevo.component';
import { FloresnuevoComponent } from './componentes/productos/floresnuevo/floresnuevo.component';
import { HerramientasnuevoComponent } from './componentes/productos/herramientasnuevo/herramientasnuevo.component';
import { PiedrasnuevoComponent } from './componentes/productos/piedrasnuevo/piedrasnuevo.component';
import { TierrasnuevoComponent } from './componentes/productos/tierrasnuevo/tierrasnuevo.component';
import { NuevoingresoComponent } from './componentes/productos/nuevoingreso/nuevoingreso.component';
import { DisenosnuevoComponent } from './componentes/productos/disenosnuevo/disenosnuevo.component';
import { AbonoComponent } from './componentes/editar/abono/abono.component';
import { ArbolComponent } from './componentes/editar/arbol/arbol.component';
import { ArbustoComponent } from './componentes/editar/arbusto/arbusto.component';
import { DecoracionesComponent } from './componentes/editar/decoracion/decoracion.component';
import { AreasComponent } from './componentes/editar/areas/areas.component';
import { CespedComponent } from './componentes/editar/cesped/cesped.component';
import { FlorComponent } from './componentes/editar/flor/flor.component';
import { HerramientaComponent } from './componentes/editar/herramienta/herramienta.component';
import { PiedraComponent } from './componentes/editar/piedra/piedra.component';
import { TierrasComponent } from './componentes/editar/tierra/tierra.component';


@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    LoginComponent,
    RegisterComponent,
    TiendaComponent,
    CatalogoComponent,
    NuevosProductosComponent,
    ContactoComponent,
    CarritoComponent,
    Navegacion1Component,
    Navegacion2Component,
    NuevosComponent,
    ListComponent,
    EditComponent,
    FooternavbarComponent,
    AbonosComponent,
    ArbolesComponent,
    ArbustosComponent,
    AreasVerdesComponent,
    CespedesComponent,
    DecoracionComponent,
    FloresComponent,
    HerramientasComponent,
    PiedrasComponent,
    TierraComponent,
    AbonosnuevoComponent,
    ArbolesnuevoComponent,
    ArbustosnuevoComponent,
    AreasVerdesnuevoComponent,
    CespedesnuevoComponent,
    DecoracionnuevoComponent,
    FloresnuevoComponent,
    HerramientasnuevoComponent,
    PiedrasnuevoComponent,
    TierrasnuevoComponent,
    NuevoingresoComponent,
    DisenosnuevoComponent,
    AbonoComponent,
    ArbolComponent,
    ArbustoComponent,
    AreasComponent,
    CespedComponent,
    FlorComponent,
    HerramientaComponent,
    PiedraComponent,
    DecoracionesComponent,
    TierrasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    PersonService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
