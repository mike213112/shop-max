export interface Decoracion {
  id?: number;
  nombre?: string;
  imagen?: string;
  descripcion?: string;
  precio?: number;
}
