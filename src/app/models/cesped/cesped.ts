export interface Cesped {
  id?: number;
  nombre?: string;
  imagen?: string;
  descripcion?: string;
  precio?: number;
}
