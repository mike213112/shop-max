export interface Person {
  id?: number;
  username?: string;
  nombre: string;
  apellido: string;
  email: string;
  password: string;
  repeat: string;
  telefono: number;
  dpi: number;
  cuidad: string;
  genero: string;
  tarjeta: number;
}
