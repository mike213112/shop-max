export interface Areasverdes {
  id?: number;
  nombre?: string;
  imagen?: string;
  descripcion?: string;
  precio?: number;
}
