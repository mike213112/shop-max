export interface Herramienta {
  id?: number;
  nombre?: string;
  imagen?: string;
  descripcion?: string;
  precio?: number;
}
