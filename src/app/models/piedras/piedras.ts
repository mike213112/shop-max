export interface Piedras {
  id?: number;
  nombre?: string;
  imagen?: string;
  descripcion?: string;
  precio?: number;
}
