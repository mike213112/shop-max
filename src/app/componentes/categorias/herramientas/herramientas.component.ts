import { Component, OnInit } from '@angular/core';

import { HerramientaService } from '../../../services/herramienta/herramienta.service';

@Component({
  selector: 'desarrolloweb-herramientas',
  templateUrl: './herramientas.component.html',
  styleUrls: ['./herramientas.component.scss']
})
export class HerramientasComponent implements OnInit {

  herramientas: any = [];

  constructor(private herramientaService: HerramientaService) { }

  ngOnInit(): void {
  }

  getHerramientas(): void {
    this.herramientaService.getHerramientasSpring()
      .subscribe(
        res => {
          this.herramientas = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este herramienta?')){
      this.herramientaService.deleteHerramientaSpring(id)
        .subscribe(
          res => {
            this.getHerramientas();
          },
          error => console.error(error)
        );
    }
  }

}
