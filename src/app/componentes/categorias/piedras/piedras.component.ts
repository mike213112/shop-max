import { Component, OnInit } from '@angular/core';

import { PiedrasService } from '../../../services/piedras/piedras.service';

@Component({
  selector: 'desarrolloweb-piedras',
  templateUrl: './piedras.component.html',
  styleUrls: ['./piedras.component.scss']
})
export class PiedrasComponent implements OnInit {

  piedras: any = [];

  constructor(private piedraService: PiedrasService) { }

  ngOnInit(): void {
    this.getPiedras();
  }

  getPiedras(): void {
    this.piedraService.getPiedrasSpring()
      .subscribe(
        res => {
          this.piedras = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este piedra?')){
      this.piedraService.deletePiedrasSpring(id)
        .subscribe(
          res => {
            this.getPiedras();
          },
          error => console.error(error)
        );
    }
  }

}
