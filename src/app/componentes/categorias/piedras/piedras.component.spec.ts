import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiedrasComponent } from './piedras.component';

describe('PiedrasComponent', () => {
  let component: PiedrasComponent;
  let fixture: ComponentFixture<PiedrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiedrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiedrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
