import { Component, OnInit } from '@angular/core';

import { TierrasService } from '../../../services/tierras/tierras.service';

@Component({
  selector: 'desarrolloweb-tierra',
  templateUrl: './tierra.component.html',
  styleUrls: ['./tierra.component.scss']
})
export class TierraComponent implements OnInit {

  tierras: any = [];

  constructor(private tierraService: TierrasService) { }

  ngOnInit(): void {
    this.getTierras();
  }

  getTierras(): void {
    this.tierraService.getTierrasSpring()
      .subscribe(
        res => {
          this.tierras = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este tierra?')){
      this.tierraService.deleteTierraSpring(id)
        .subscribe(
          res => {
            this.getTierras();
          },
          error => console.error(error)
        );
    }
  }

}
