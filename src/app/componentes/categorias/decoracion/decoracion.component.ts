import { Component, OnInit } from '@angular/core';

import { DecoracionService } from '../../../services/decoracion/decoracion.service';

@Component({
  selector: 'desarrolloweb-decoracion',
  templateUrl: './decoracion.component.html',
  styleUrls: ['./decoracion.component.scss']
})
export class DecoracionComponent implements OnInit {

  decoraciones: any = [];

  constructor(private decoracionService: DecoracionService) { }

  ngOnInit(): void {
    this.getDecoraciones();
  }

  getDecoraciones(): void {
    this.decoracionService.getDecoracionesSpring()
      .subscribe(
        res => {
          this.decoraciones = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este decoracion?')){
      this.decoracionService.deleteDecoracionSpring(id)
        .subscribe(
          res => {
            this.getDecoraciones();
          },
          error => console.error(error)
        );
    }
  }

}
