import { Component, OnInit } from '@angular/core';

import { AbonoService } from '../../../services/abono/abono.service';

@Component({
  selector: 'desarrolloweb-abonos',
  templateUrl: './abonos.component.html',
  styleUrls: ['./abonos.component.scss']
})
export class AbonosComponent implements OnInit {

  abonos: any = [];

  constructor(private abonosService: AbonoService) { }

  ngOnInit(): void {
    this.getAbonos();
  }

  getAbonos(): void {
    this.abonosService.getAbonosSpring()
      .subscribe(
        res => {
          this.abonos = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este abono?')){
      this.abonosService.deleteAbonosSpring(id)
        .subscribe(
          res => {
            this.getAbonos();
          },
          error => console.error(error)
        );
    }
  }

}
