import { Component, OnInit } from '@angular/core';

import { ArbolService } from '../../../services/arbol/arbol.service';

@Component({
  selector: 'desarrolloweb-arboles',
  templateUrl: './arboles.component.html',
  styleUrls: ['./arboles.component.scss']
})
export class ArbolesComponent implements OnInit {

  arboles: any = [];

  constructor(private arbolService: ArbolService) { }

  ngOnInit(): void {
    this.getArboles();
  }

  getArboles(): void {
    this.arbolService.getArbolesSpring()
      .subscribe(
        res => {
          this.arboles = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este arbol?')){
      this.arbolService.deleteArbolSpring(id)
        .subscribe(
          res => {
            this.getArboles();
          },
          error => console.error(error)
        );
    }
  }

}
