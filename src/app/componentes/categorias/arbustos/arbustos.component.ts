import { Component, OnInit } from '@angular/core';

import { ArbustosService } from '../../../services/arbustos/arbustos.service';

@Component({
  selector: 'desarrolloweb-arbustos',
  templateUrl: './arbustos.component.html',
  styleUrls: ['./arbustos.component.scss']
})
export class ArbustosComponent implements OnInit {

  arbustos: any = [];

  constructor(private arbustosService: ArbustosService) { }

  ngOnInit(): void {
    this.getArbustos();
  }

  getArbustos(): void {
    this.arbustosService.getArbustosSpring()
      .subscribe(
        res => {
          this.arbustos = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este arbusto?')){
      this.arbustosService.deleteArbustosSpring(id)
        .subscribe(
          res => {
            this.getArbustos();
          },
          error => console.error(error)
        );
    }
  }

}
