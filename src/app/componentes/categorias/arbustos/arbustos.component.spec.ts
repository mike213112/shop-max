import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbustosComponent } from './arbustos.component';

describe('ArbustosComponent', () => {
  let component: ArbustosComponent;
  let fixture: ComponentFixture<ArbustosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbustosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbustosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
