import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasVerdesComponent } from './areas-verdes.component';

describe('AreasVerdesComponent', () => {
  let component: AreasVerdesComponent;
  let fixture: ComponentFixture<AreasVerdesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreasVerdesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreasVerdesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
