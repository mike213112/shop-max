import { Component, OnInit } from '@angular/core';

import { AreasService } from '../../../services/areas/areas.service';

@Component({
  selector: 'desarrolloweb-areas-verdes',
  templateUrl: './areas-verdes.component.html',
  styleUrls: ['./areas-verdes.component.scss']
})
export class AreasVerdesComponent implements OnInit {

  areas: any = [];

  constructor(private areasService: AreasService) { }

  ngOnInit(): void {
    this.getAreas();
  }

  getAreas(): void {
    this.areasService.getAreasVerdesSpring()
      .subscribe(
        res => {
          this.areas = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar esta area verde?')){
      this.areasService.deleteAreasverdesSpring(id)
        .subscribe(
          res => {
            this.getAreas();
          },
          error => console.error(error)
        );
    }
  }

}
