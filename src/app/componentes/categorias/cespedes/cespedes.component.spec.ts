import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CespedesComponent } from './cespedes.component';

describe('CespedesComponent', () => {
  let component: CespedesComponent;
  let fixture: ComponentFixture<CespedesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CespedesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CespedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
