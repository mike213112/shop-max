import { Component, OnInit } from '@angular/core';

import { CespedService } from '../../../services/cesped/cesped.service';

@Component({
  selector: 'desarrolloweb-cespedes',
  templateUrl: './cespedes.component.html',
  styleUrls: ['./cespedes.component.scss']
})
export class CespedesComponent implements OnInit {

  cespedes: any = [];

  constructor(private cespedService: CespedService) { }

  ngOnInit(): void {
    this.getCespedes();
  }

  getCespedes(): void {
    this.cespedService.getCespedesSpring()
      .subscribe(
        res => {
          this.cespedes = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este cesped?')){
      this.cespedService.deleteCespedSpring(id)
        .subscribe(
          res => {
            this.getCespedes();
          },
          error => console.error(error)
        );
    }
  }

}
