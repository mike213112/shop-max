import { Component, OnInit } from '@angular/core';

import { FloresService } from '../../../services/flores/flores.service';

@Component({
  selector: 'desarrolloweb-flores',
  templateUrl: './flores.component.html',
  styleUrls: ['./flores.component.scss']
})
export class FloresComponent implements OnInit {

  flores: any = [];

  constructor(private floresServices: FloresService) { }

  ngOnInit(): void {
    this.getFlores();
  }

  getFlores(): void {
    this.floresServices.getFloresSpring()
      .subscribe(
        res => {
          this.flores = res;
        },
        error => console.error(error)
      );
  }

  delete(id: string): void {
    if (confirm('Estas seguro de eliminar este flor?')){
      this.floresServices.deleteFloreSpring(id)
        .subscribe(
          res => {
            this.getFlores();
          },
          error => console.error(error)
        );
    }
  }

}
