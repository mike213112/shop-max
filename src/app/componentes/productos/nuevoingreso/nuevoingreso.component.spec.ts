import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoingresoComponent } from './nuevoingreso.component';

describe('NuevoingresoComponent', () => {
  let component: NuevoingresoComponent;
  let fixture: ComponentFixture<NuevoingresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoingresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoingresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
