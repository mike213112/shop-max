import { Component, OnInit } from '@angular/core';

import { Piedras } from '../../../models/piedras/piedras';
import { PiedrasService } from '../../../services/piedras/piedras.service';

@Component({
  selector: 'desarrolloweb-piedrasnuevo',
  templateUrl: './piedrasnuevo.component.html',
  styleUrls: ['./piedrasnuevo.component.scss']
})
export class PiedrasnuevoComponent implements OnInit {

  piedras: Piedras = {
    nombre: '',
    imagen: '',
    precio: 0,
    descripcion: ''
  };

  constructor(private piedrasServices: PiedrasService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.piedras.id;
    this.piedrasServices.createPiedrasSpring(this.piedras)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
