import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiedrasnuevoComponent } from './piedrasnuevo.component';

describe('PiedrasnuevoComponent', () => {
  let component: PiedrasnuevoComponent;
  let fixture: ComponentFixture<PiedrasnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiedrasnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiedrasnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
