import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HerramientasnuevoComponent } from './herramientasnuevo.component';

describe('HerramientasnuevoComponent', () => {
  let component: HerramientasnuevoComponent;
  let fixture: ComponentFixture<HerramientasnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HerramientasnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HerramientasnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
