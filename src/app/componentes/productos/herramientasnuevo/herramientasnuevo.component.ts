import { Component, OnInit } from '@angular/core';

import { Herramienta } from '../../../models/herramienta/herramienta';
import { HerramientaService } from '../../../services/herramienta/herramienta.service';

@Component({
  selector: 'desarrolloweb-herramientasnuevo',
  templateUrl: './herramientasnuevo.component.html',
  styleUrls: ['./herramientasnuevo.component.scss']
})
export class HerramientasnuevoComponent implements OnInit {

  herramientas: Herramienta = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  }

  constructor(private herramientaService: HerramientaService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.herramientas.id;
    this.herramientaService.createHerramientaSpring(this.herramientas)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
