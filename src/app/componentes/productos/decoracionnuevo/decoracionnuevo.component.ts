import { Component, OnInit } from '@angular/core';

import { Decoracion } from '../../../models/decoracion/decoracion';
import { DecoracionService } from '../../../services/decoracion/decoracion.service';

@Component({
  selector: 'desarrolloweb-decoracionnuevo',
  templateUrl: './decoracionnuevo.component.html',
  styleUrls: ['./decoracionnuevo.component.scss']
})
export class DecoracionnuevoComponent implements OnInit {

  decoracion: Decoracion = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private decoracionService: DecoracionService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.decoracion.id;
    this.decoracionService.createDecoracionSpring(this.decoracion)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
