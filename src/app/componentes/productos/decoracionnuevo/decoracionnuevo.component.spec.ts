import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecoracionnuevoComponent } from './decoracionnuevo.component';

describe('DecoracionnuevoComponent', () => {
  let component: DecoracionnuevoComponent;
  let fixture: ComponentFixture<DecoracionnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecoracionnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecoracionnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
