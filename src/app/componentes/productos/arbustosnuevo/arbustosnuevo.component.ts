import { Component, OnInit } from '@angular/core';

import { Arbustos } from '../../../models/arbustos/arbustos';
import { ArbustosService } from '../../../services/arbustos/arbustos.service';

@Component({
  selector: 'desarrolloweb-arbustosnuevo',
  templateUrl: './arbustosnuevo.component.html',
  styleUrls: ['./arbustosnuevo.component.scss']
})
export class ArbustosnuevoComponent implements OnInit {

  arbustos: Arbustos = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private arbustosServices: ArbustosService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.arbustos.id;
    this.arbustosServices.createArbustosSpring(this.arbustos)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
