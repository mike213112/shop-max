import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbustosnuevoComponent } from './arbustosnuevo.component';

describe('ArbustosnuevoComponent', () => {
  let component: ArbustosnuevoComponent;
  let fixture: ComponentFixture<ArbustosnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbustosnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbustosnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
