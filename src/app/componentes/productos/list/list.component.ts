import { ProductService } from './../../../services/producto/product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'desarrolloweb-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  products: any = [];

  constructor(private productService: ProductService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    //this.getProducts();
    this.getProductswithSpring();
  }


  // tslint:disable-next-line:typedef
  getProducts() {
    this.productService.getProducts()
      .subscribe(
        res => {
          this.products = res;
        },
        err => console.error(err)
      );
  }

  // tslint:disable-next-line:typedef
  getProductswithSpring() {
    this.productService.getProductswithspring()
      .subscribe(
        res => {
          this.products = res;
        },
        err => console.error(err)
      );
  }

  // tslint:disable-next-line:typedef
  delete(id: string) {
    if (confirm('Estas seguro de eliminar este producto?')) {
      this.productService.deleteProduct(id)
      .subscribe(
        res => {
          console.log(res);
          this.getProducts();
        },
        err => console.error(err)
      );
    }
  }

  // tslint:disable-next-line:typedef
  deletewithSpring(id: string) {
    if (confirm('Estas seguro de eliminar este producto?')) {
      this.productService.deleteProductwithspring(id)
      .subscribe(
        res => {
          console.log(res);
          this.getProductswithSpring();
        },
        err => console.error(err)
      );
    }
  }

}
