import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasVerdesnuevoComponent } from './areas-verdesnuevo.component';

describe('AreasVerdesnuevoComponent', () => {
  let component: AreasVerdesnuevoComponent;
  let fixture: ComponentFixture<AreasVerdesnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreasVerdesnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreasVerdesnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
