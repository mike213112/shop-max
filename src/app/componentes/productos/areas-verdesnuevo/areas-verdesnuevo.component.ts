import { Component, OnInit } from '@angular/core';

import { Areasverdes } from '../../../models/areas/areasverdes';
import { AreasService } from '../../../services/areas/areas.service';

@Component({
  selector: 'desarrolloweb-areas-verdesnuevo',
  templateUrl: './areas-verdesnuevo.component.html',
  styleUrls: ['./areas-verdesnuevo.component.scss']
})
export class AreasVerdesnuevoComponent implements OnInit {

  areas: Areasverdes = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private areasService: AreasService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.areas.id;
    this.areasService.createAreaverdeSpring(this.areas)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
