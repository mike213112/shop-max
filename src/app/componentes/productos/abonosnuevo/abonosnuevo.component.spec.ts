import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonosnuevoComponent } from './abonosnuevo.component';

describe('AbonosnuevoComponent', () => {
  let component: AbonosnuevoComponent;
  let fixture: ComponentFixture<AbonosnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonosnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonosnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
