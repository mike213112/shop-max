import { Component, OnInit } from '@angular/core';
import {Abono} from '../../../models/abono/abono';
import {AbonoService} from '../../../services/abono/abono.service';
import {Router} from '@angular/router';

@Component({
  selector: 'desarrolloweb-abonosnuevo',
  templateUrl: './abonosnuevo.component.html',
  styleUrls: ['./abonosnuevo.component.scss']
})
export class AbonosnuevoComponent implements OnInit {

  abonos: Abono = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private abonosService: AbonoService,
              private router: Router) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.abonos.id;
    this.abonosService.createAbonoSpring(this.abonos)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
