import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbolesnuevoComponent } from './arbolesnuevo.component';

describe('ArbolesnuevoComponent', () => {
  let component: ArbolesnuevoComponent;
  let fixture: ComponentFixture<ArbolesnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbolesnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbolesnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
