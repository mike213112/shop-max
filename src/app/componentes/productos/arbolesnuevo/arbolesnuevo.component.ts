import { Component, OnInit } from '@angular/core';
import { Arbol } from '../../../models/arbol/arbol';
import { ArbolService } from '../../../services/arbol/arbol.service';

@Component({
  selector: 'desarrolloweb-arbolesnuevo',
  templateUrl: './arbolesnuevo.component.html',
  styleUrls: ['./arbolesnuevo.component.scss']
})
export class ArbolesnuevoComponent implements OnInit {

  arboles: Arbol = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  }

  constructor(private arbolesServices: ArbolService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.arboles.id;
    this.arbolesServices.createArbolSpring(this.arboles)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
