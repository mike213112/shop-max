import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CespedesnuevoComponent } from './cespedesnuevo.component';

describe('CespedesnuevoComponent', () => {
  let component: CespedesnuevoComponent;
  let fixture: ComponentFixture<CespedesnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CespedesnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CespedesnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
