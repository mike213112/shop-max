import { Component, OnInit } from '@angular/core';

import { Cesped } from '../../../models/cesped/cesped';
import { CespedService } from '../../../services/cesped/cesped.service';

@Component({
  selector: 'desarrolloweb-cespedesnuevo',
  templateUrl: './cespedesnuevo.component.html',
  styleUrls: ['./cespedesnuevo.component.scss']
})
export class CespedesnuevoComponent implements OnInit {

  cesped: Cesped = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private cespedService: CespedService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.cesped.id;
    this.cespedService.createCespedSpring(this.cesped)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
