import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

import { Product } from './../../../models/producto/product';
import { ProductService } from './../../../services/producto/product.service';

@Component({
  selector: 'desarrolloweb-nuevos',
  templateUrl: './nuevos.component.html',
  styleUrls: ['./nuevos.component.scss']
})
export class NuevosComponent implements OnInit {

  product: Product = {
    nombre: '',
    precio: 0,
    descripcion: '',
    imagen: ''
  };

  edit = false;

  constructor(private productService: ProductService,
              private router: Router,
              private activatedRouter: ActivatedRoute,
              private toast: ToastrService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
    //   this.productService.getProduct(parametros.id)
    //     .subscribe(
    //       (res) => {
    //       console.log(res);
    //       console.log(JSON.stringify(res));
    //       this.product = res;
    //       this.edit = true;
    //     },
    //     (err) => console.log(err)
    //   );
      this.productService.getProductwithspring(parametros.id)
        .subscribe(
          (res) => {
          console.log(res);
          console.log(JSON.stringify(res));
          this.product = res;
          this.edit = true;
        },
        (err) => console.log(err)
      );
    }

  }
  // tslint:disable-next-line:typedef
  // GuardarProduct(){
  //   delete this.product.id;
  //   this.productService.createProduct(this.product)
  //     .subscribe( res => {
  //       console.log(res);
  //       this.router.navigate(['/list']);
  //     });
  // }

  // tslint:disable-next-line:typedef
  GuardarProductwithSpring(){
    delete this.product.id;
    this.productService.createProductwithspring(this.product)
      .subscribe( res => {
        console.log(res);
        this.router.navigate(['/list']);
      });
  }

  // tslint:disable-next-line:typedef
  // updateProduct(){
  //   this.productService.updateProduct(this.product.id, this.product)
  //     .subscribe(
  //       res => {
  //         console.log(res);
  //         this.router.navigate(['/list']);
  //       },
  //       err => console.error(err)
  //     );
  // }

  // tslint:disable-next-line:typedef
  // updateProductwithSpring(){
  //   this.productService.updateProductwithspring(this.product.id, this.product)
  //     .subscribe(
  //       res => {
  //         console.log(res);
  //         this.router.navigate(['/list']);
  //       },
  //       err => console.error(err)
  //     );
  // }

}
