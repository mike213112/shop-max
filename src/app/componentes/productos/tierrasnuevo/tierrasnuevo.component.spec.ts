import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TierrasnuevoComponent } from './tierrasnuevo.component';

describe('TierrasnuevoComponent', () => {
  let component: TierrasnuevoComponent;
  let fixture: ComponentFixture<TierrasnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TierrasnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TierrasnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
