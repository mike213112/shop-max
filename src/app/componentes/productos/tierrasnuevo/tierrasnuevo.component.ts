import { Component, OnInit } from '@angular/core';

import { Tierras } from '../../../models/tierras/tierras';
import { TierrasService } from '../../../services/tierras/tierras.service';

@Component({
  selector: 'desarrolloweb-tierrasnuevo',
  templateUrl: './tierrasnuevo.component.html',
  styleUrls: ['./tierrasnuevo.component.scss']
})
export class TierrasnuevoComponent implements OnInit {

  tierras: Tierras = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private tierrasService: TierrasService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.tierras.id;
    this.tierrasService.createTierraSpring(this.tierras)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
