import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Product } from 'src/app/models/producto/product';
import { ProductService } from 'src/app/services/producto/product.service';

@Component({
  selector: 'desarrolloweb-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  product: Product = {
    id: 0,
    precio: 0,
    descripcion: '',
    imagen: ''
  };

  constructor(private productService: ProductService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
    //   console.log(parametros.id);
    //   this.productService.getProduct(parametros.id)
    //     .subscribe(res => {
    //       console.log(JSON.stringify(res));
    //       this.product = res as Product;
    //     },
    //       err => console.log(err)
    //     );
      this.productService.getProductwithspring(parametros.id)
        .subscribe(res => {
          console.log(JSON.stringify(res));
          this.product = res as Product;
        },
          err => console.log(err)
        );
    }
  }

  // tslint:disable-next-line:typedef
  // updateProduct(formulario: NgForm) {
  //   const llave = this.activatedRouter.snapshot.params;
  //   this.productService.updateProduct(llave.id, formulario.value)
  //     .subscribe(
  //       res => {
  //         console.log(res);
  //         this.router.navigate(['/list']);
  //       },
  //       err => console.error(err)
  //     );
  // }

  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.productService.updateProductwithspring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/list']);
        },
        err => console.error(err)
      );
  }

}
