import { Component, OnInit } from '@angular/core';

import { Disenos } from '../../../models/disenos/disenos';
import { DisenosService } from '../../../services/disenos/disenos.service';

@Component({
  selector: 'desarrolloweb-disenosnuevo',
  templateUrl: './disenosnuevo.component.html',
  styleUrls: ['./disenosnuevo.component.scss']
})
export class DisenosnuevoComponent implements OnInit {

  disenos: Disenos = {
    nombre: '',
    descripcion: '',
    imagen: '',
    precio: 0
  };

  constructor(private disenosServices: DisenosService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.disenos.id;
    this.disenosServices.createDisenoSpring(this.disenos)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
