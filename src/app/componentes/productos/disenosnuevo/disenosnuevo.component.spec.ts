import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisenosnuevoComponent } from './disenosnuevo.component';

describe('DisenosnuevoComponent', () => {
  let component: DisenosnuevoComponent;
  let fixture: ComponentFixture<DisenosnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisenosnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisenosnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
