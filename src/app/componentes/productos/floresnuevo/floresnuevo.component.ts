import { Component, OnInit } from '@angular/core';

import { Flores } from '../../../models/flores/flores';
import { FloresService } from '../../../services/flores/flores.service';

@Component({
  selector: 'desarrolloweb-floresnuevo',
  templateUrl: './floresnuevo.component.html',
  styleUrls: ['./floresnuevo.component.scss']
})
export class FloresnuevoComponent implements OnInit {

  flores: Flores = {
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private floresService: FloresService) { }

  ngOnInit(): void {
  }

  GuardarPublicacionSpring(): void{
    delete this.flores.id;
    this.floresService.createFloreSpring(this.flores)
      .subscribe(
        res => {
          console.log(res);
        },
        error => console.error(error)
      );
  }

}
