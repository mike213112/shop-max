import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloresnuevoComponent } from './floresnuevo.component';

describe('FloresnuevoComponent', () => {
  let component: FloresnuevoComponent;
  let fixture: ComponentFixture<FloresnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloresnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloresnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
