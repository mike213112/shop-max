import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Navegacion2Component } from './navegacion2.component';

describe('Navegacion2Component', () => {
  let component: Navegacion2Component;
  let fixture: ComponentFixture<Navegacion2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Navegacion2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Navegacion2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
