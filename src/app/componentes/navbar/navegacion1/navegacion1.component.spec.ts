import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Navegacion1Component } from './navegacion1.component';

describe('Navegacion1Component', () => {
  let component: Navegacion1Component;
  let fixture: ComponentFixture<Navegacion1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Navegacion1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Navegacion1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
