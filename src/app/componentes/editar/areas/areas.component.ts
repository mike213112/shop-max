import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Areasverdes } from '../../../models/areas/areasverdes';
import { AreasService } from '../../../services/areas/areas.service';

@Component({
  selector: 'desarrolloweb-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {

  area: Areasverdes = {
    id: 0,
    nombre: '',
    imagen: '',
    precio: 0,
    descripcion: ''
  };

  constructor(private areasService: AreasService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.areasService.getAreaVerdeSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.area = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.areasService.updateAreasverdesSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/areas']);
        },
        err => console.error(err)
      );
  }

}
