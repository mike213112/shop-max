import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Herramienta } from '../../../models/herramienta/herramienta';
import { HerramientaService } from '../../../services/herramienta/herramienta.service';

@Component({
  selector: 'desarrolloweb-herramienta',
  templateUrl: './herramienta.component.html',
  styleUrls: ['./herramienta.component.scss']
})
export class HerramientaComponent implements OnInit {

  herramienta: Herramienta = {
    id: 0,
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private herramientaService: HerramientaService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.herramientaService.getHerramientaSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.herramienta = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.herramientaService.updateHerramientaSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/herramienta']);
        },
        err => console.error(err)
      );
  }

}
