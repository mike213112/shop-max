import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Arbustos } from '../../../models/arbustos/arbustos';
import { ArbustosService } from '../../../services/arbustos/arbustos.service';

@Component({
  selector: 'desarrolloweb-arbusto',
  templateUrl: './arbusto.component.html',
  styleUrls: ['./arbusto.component.scss']
})
export class ArbustoComponent implements OnInit {

  arbusto: Arbustos = {
    id: 0,
    nombre: '',
    imagen: '',
    precio: 0,
    descripcion: ''
  };

  constructor(private arbustoService: ArbustosService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.arbustoService.getArbustoSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.arbusto = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.arbustoService.updateArbustosSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/arbusto']);
        },
        err => console.error(err)
      );
  }

}
