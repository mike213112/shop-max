import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbustoComponent } from './arbusto.component';

describe('ArbustoComponent', () => {
  let component: ArbustoComponent;
  let fixture: ComponentFixture<ArbustoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbustoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbustoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
