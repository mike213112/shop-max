import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Flores } from '../../../models/flores/flores';
import { FloresService } from '../../../services/flores/flores.service';

@Component({
  selector: 'desarrolloweb-flor',
  templateUrl: './flor.component.html',
  styleUrls: ['./flor.component.scss']
})
export class FlorComponent implements OnInit {

  flor: Flores = {
    id: 0,
    nombre: '',
    imagen: '',
    precio: 0,
    descripcion: ''
  };

  constructor(private florService: FloresService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.florService.getFloreSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.flor = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.florService.updateFloreSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/flor']);
        },
        err => console.error(err)
      );
  }

}
