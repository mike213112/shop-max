import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Decoracion } from '../../../models/decoracion/decoracion';
import { DecoracionService } from '../../../services/decoracion/decoracion.service';

@Component({
  selector: 'desarrolloweb-decoracion',
  templateUrl: './decoracion.component.html',
  styleUrls: ['./decoracion.component.scss']
})
export class DecoracionesComponent implements OnInit {

  decoracion: Decoracion = {
    id: 0,
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  }

  constructor(private decoracionService: DecoracionService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.decoracionService.getDecoracionSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.decoracion = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.decoracionService.updateDecoracionSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/decoracion']);
        },
        err => console.error(err)
      );
  }

}
