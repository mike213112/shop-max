import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Tierras } from '../../../models/tierras/tierras';
import { TierrasService } from '../../../services/tierras/tierras.service';

@Component({
  selector: 'desarrolloweb-tierra',
  templateUrl: './tierra.component.html',
  styleUrls: ['./tierra.component.scss']
})
export class TierrasComponent implements OnInit {

  tierra: Tierras = {
    id: 0,
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private tierraService: TierrasService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.tierraService.getTierraSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.tierra = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.tierraService.updateTierraSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/tierra']);
        },
        err => console.error(err)
      );
  }

}
