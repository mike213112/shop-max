import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Abono } from '../../../models/abono/abono';
import { AbonoService } from '../../../services/abono/abono.service';

@Component({
  selector: 'desarrolloweb-abono',
  templateUrl: './abono.component.html',
  styleUrls: ['./abono.component.scss']
})
export class AbonoComponent implements OnInit {

  abonos: Abono = {
    id: 0,
    nombre: '',
    imagen: '',
    descripcion: '',
    precio: 0
  };

  constructor(private abonoService: AbonoService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.abonoService.getAbonoSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.abonos = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.abonoService.updateAbonosSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/abono']);
        },
        err => console.error(err)
      );
  }
}
