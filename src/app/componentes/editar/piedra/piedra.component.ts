import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Piedras } from '../../../models/piedras/piedras';
import { PiedrasService } from '../../../services/piedras/piedras.service';

@Component({
  selector: 'desarrolloweb-piedra',
  templateUrl: './piedra.component.html',
  styleUrls: ['./piedra.component.scss']
})
export class PiedraComponent implements OnInit {

  piedra: Piedras = {
    id: 0,
    nombre: '',
    imagen: '',
    precio: 0,
    descripcion: ''
  };

  constructor(private piedraService: PiedrasService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.piedraService.getPiedraSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.piedra = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.piedraService.updatePiedrasSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/piedra']);
        },
        err => console.error(err)
      );
  }

}
