import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Cesped } from '../../../models/cesped/cesped';
import { CespedService } from '../../../services/cesped/cesped.service';

@Component({
  selector: 'desarrolloweb-cesped',
  templateUrl: './cesped.component.html',
  styleUrls: ['./cesped.component.scss']
})
export class CespedComponent implements OnInit {

  cesped: Cesped = {
    id: 0,
    nombre: '',
    imagen: '',
    precio: 0,
    descripcion: ''
  };

  constructor(private cespedService: CespedService,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    const parametros = this.activatedRouter.snapshot.params;
    if (parametros.id) {
      //   console.log(parametros.id);
      this.cespedService.getCespedSpring(parametros.id)
        .subscribe(res => {
            console.log(JSON.stringify(res));
            this.cesped = res;
          },
          err => console.log(err)
        );
    }
  }
  // tslint:disable-next-line:typedef
  updateProductwithSpring(formulario: NgForm) {
    const llave = this.activatedRouter.snapshot.params;
    this.cespedService.updateCespedSpring(formulario.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/catalogo/cesped']);
        },
        err => console.error(err)
      );
  }

}
