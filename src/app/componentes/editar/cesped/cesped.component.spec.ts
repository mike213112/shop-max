import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CespedComponent } from './cesped.component';

describe('CespedComponent', () => {
  let component: CespedComponent;
  let fixture: ComponentFixture<CespedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CespedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CespedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
