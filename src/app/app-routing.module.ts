import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarritoComponent } from './componentes/principal/carrito/carrito.component';
import { NuevosProductosComponent } from './componentes/principal/nuevos-productos/nuevos-productos.component';
import { CatalogoComponent } from './componentes/principal/catalogo/catalogo.component';
import { ContactoComponent } from './componentes/principal/contacto/contacto.component';
import { TiendaComponent } from './componentes/principal/tienda/tienda.component';
import { RegisterComponent } from './componentes/user/register/register.component';
import { LoginComponent } from './componentes/user/login/login.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { ListComponent } from './componentes/productos/list/list.component';
import { NuevosComponent } from './componentes/productos/nuevos/nuevos.component';
import { EditComponent } from './componentes/productos/edit/edit.component';
import {AbonosComponent} from './componentes/categorias/abonos/abonos.component';
import {ArbolesComponent} from './componentes/categorias/arboles/arboles.component';
import {AreasVerdesComponent} from './componentes/categorias/areas-verdes/areas-verdes.component';
import {ArbustosComponent} from './componentes/categorias/arbustos/arbustos.component';
import {CespedesComponent} from './componentes/categorias/cespedes/cespedes.component';
import {DecoracionComponent} from './componentes/categorias/decoracion/decoracion.component';
import {FloresComponent} from './componentes/categorias/flores/flores.component';
import {HerramientasComponent} from './componentes/categorias/herramientas/herramientas.component';
import {PiedrasComponent} from './componentes/categorias/piedras/piedras.component';
import {TierraComponent} from './componentes/categorias/tierra/tierra.component';
import {NuevoingresoComponent} from './componentes/productos/nuevoingreso/nuevoingreso.component';

import { AbonoComponent } from './componentes/editar/abono/abono.component';
import { ArbolComponent } from './componentes/editar/arbol/arbol.component';
import { ArbustoComponent } from './componentes/editar/arbusto/arbusto.component';
import { AreasComponent } from './componentes/editar/areas/areas.component';
import { CespedComponent } from './componentes/editar/cesped/cesped.component';
import { DecoracionesComponent } from './componentes/editar/decoracion/decoracion.component';
import { FlorComponent } from './componentes/editar/flor/flor.component';
import { HerramientaComponent } from './componentes/editar/herramienta/herramienta.component';
import { PiedraComponent } from './componentes/editar/piedra/piedra.component';
import { TierrasComponent } from './componentes/editar/tierra/tierra.component';

const routes: Routes = [
  { path: 'principal', component: PrincipalComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'diseños', component: TiendaComponent },
  { path: 'contacto', component: ContactoComponent },
 // { path: 'produtos', component: CatalogoComponent },
  { path: 'catalogo', component: CatalogoComponent },
  { path: 'nuevos-productos', component: NuevosProductosComponent },
  { path: 'carrito', component: CarritoComponent },
  { path: 'producto/crear', component: NuevosComponent },
  { path: 'list', component: ListComponent },
  { path: 'product/edit/:id', component: EditComponent },
  { path: 'catalogo/abonos', component: AbonosComponent },
  { path: 'catalogo/arboles', component: ArbolesComponent },
  { path: 'catalogo/areas-verdes', component: AreasVerdesComponent },
  { path: 'catalogo/arbustos', component: ArbustosComponent },
  { path: 'catalogo/cespedes-o-grama', component: CespedesComponent },
  { path: 'catalogo/decoraciones', component: DecoracionComponent },
  { path: 'catalogo/flores', component: FloresComponent },
  { path: 'catalogo/herramientas', component: HerramientasComponent },
  { path: 'catalogo/piedras', component: PiedrasComponent },
  { path: 'nuevoingreso', component: NuevoingresoComponent },
  { path: 'catalogo/tierras', component: TierraComponent },
  { path: 'abono/edit/:id', component: AbonoComponent },
  { path: 'arbol/edit/:id', component: ArbolComponent },
  { path: 'arbusto/edit/:id', component: ArbustoComponent },
  { path: 'areas/edit/:id', component: AreasComponent },
  { path: 'cesped/edit/:id', component: CespedComponent },
  { path: 'decoracion/edit/:id', component: DecoracionesComponent },
  { path: 'flor/edit/:id', component: FlorComponent },
  { path: 'herramienta/edit/:id', component: HerramientaComponent },
  { path: 'piedra/edit/:id', component: PiedraComponent },
  { path: 'tierra/edit/:id', component: TierrasComponent },
  // tslint:disable-next-line: whitespace
  { path: '**', pathMatch:'full', redirectTo:'principal' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
