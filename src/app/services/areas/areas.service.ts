import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Areasverdes } from '../../models/areas/areasverdes';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/areasverdes';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getAreasverdes() {
    return this.http.get(`${this.url}/areasverdes`);
  }

  // tslint:disable-next-line:typedef
  getAreaverde(id: string) {
    return this.http.get(`${this.url}/areasverdes/${id}`);
  }

  // tslint:disable-next-line:typedef
  createAreaverde(areas: Areasverdes) {
    return this.http.post(`${this.url}/areasverdes`, areas);
  }

  // tslint:disable-next-line:typedef
  updateAreaverde(id: string, areas: Areasverdes) {
    return this.http.put(`${this.url}/areasverdes/${id}`, areas);
  }

  // tslint:disable-next-line:typedef
  deleteAreaverde(id: string) {
    return this.http.delete(`${this.url}/areasverdes/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getAreasVerdesSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getAreaVerdeSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createAreaverdeSpring(areas: Areasverdes){
    return this.http.post(`${this.url1}/create`, areas);
  }

  // tslint:disable-next-line:typedef
  updateAreasverdesSpring(updateareas: Areasverdes): Observable<Areasverdes>{
    return this.http.put(`${this.url1}/update`, updateareas);
  }

  // tslint:disable-next-line:typedef
  deleteAreasverdesSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
