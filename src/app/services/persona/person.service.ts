import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Person } from '../../models/persona/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  url = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line: typedef
  getPersons(){
    return this.http.get(`${this.url}/person`);
  }

  // tslint:disable-next-line:typedef
  getPerson(id: string){
    return this.http.get(`${this.url}/person/${id}`);
  }

  // tslint:disable-next-line:typedef
  createPerson(person: Person){
    return this.http.post(`${this.url}/person`, person);
  }

  // tslint:disable-next-line:typedef
  updatePerson(id: string, person: Person){
    return this.http.put(`${this.url}/person/${id}`, person);
  }

  // tslint:disable-next-line:typedef
  deletePerson(id: string){
    return this.http.delete(`${this.url}/person/${id}`);
  }

//####################################################################################################
// Spring boot
//####################################################################################################


}
