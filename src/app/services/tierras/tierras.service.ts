import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tierras } from '../../models/tierras/tierras';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TierrasService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/tierras';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getTierras() {
    return this.http.get(`${this.url}/tierras`);
  }

  // tslint:disable-next-line:typedef
  getTierra(id: string) {
    return this.http.get(`${this.url}/tierras/${id}`);
  }

  // tslint:disable-next-line:typedef
  createTierra(tierras: Tierras) {
    return this.http.post(`${this.url}/tierras`, tierras);
  }

  // tslint:disable-next-line:typedef
  updateTierra(id: string, tierras: Tierras) {
    return this.http.put(`${this.url}/tierras/${id}`, tierras);
  }

  // tslint:disable-next-line:typedef
  deleteTierra(id: string) {
    return this.http.delete(`${this.url}/tierras/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getTierrasSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getTierraSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createTierraSpring(tierras: Tierras){
    return this.http.post(`${this.url1}/create`, tierras);
  }

  // tslint:disable-next-line:typedef
  updateTierraSpring(updatetierras: Tierras): Observable<Tierras>{
    return this.http.put(`${this.url1}/update`, updatetierras);
  }

  // tslint:disable-next-line:typedef
  deleteTierraSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
