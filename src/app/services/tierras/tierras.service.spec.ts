import { TestBed } from '@angular/core/testing';

import { TierrasService } from './tierras.service';

describe('TierrasService', () => {
  let service: TierrasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TierrasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
