import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Decoracion } from '../../models/decoracion/decoracion';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DecoracionService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/decoracion';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getDecoraciones() {
    return this.http.get(`${this.url}/decoracion`);
  }

  // tslint:disable-next-line:typedef
  getDecoracion(id: string) {
    return this.http.get(`${this.url}/decoracion/${id}`);
  }

  // tslint:disable-next-line:typedef
  createDecoracion(decoracion: Decoracion) {
    return this.http.post(`${this.url}/decoracion`, decoracion);
  }

  // tslint:disable-next-line:typedef
  updateDecoracion(id: string, decoracion: Decoracion) {
    return this.http.put(`${this.url}/decoracion/${id}`, decoracion);
  }

  // tslint:disable-next-line:typedef
  deleteDecoracion(id: string) {
    return this.http.delete(`${this.url}/decoracion/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getDecoracionesSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getDecoracionSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createDecoracionSpring(decoracion: Decoracion){
    return this.http.post(`${this.url1}/create`, decoracion);
  }

  // tslint:disable-next-line:typedef
  updateDecoracionSpring(updatedecoracion: Decoracion): Observable<Decoracion>{
    return this.http.put(`${this.url1}/update`, updatedecoracion);
  }

  // tslint:disable-next-line:typedef
  deleteDecoracionSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
