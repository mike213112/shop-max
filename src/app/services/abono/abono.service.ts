import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Abono } from '../../models/abono/abono';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AbonoService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2222/abonos';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getAbonos() {
    return this.http.get(`${this.url}/abonos`);
  }

  // tslint:disable-next-line:typedef
  getAbono(id: string) {
    return this.http.get(`${this.url}/abonos/${id}`);
  }

  // tslint:disable-next-line:typedef
  createAbono(abono: Abono) {
    return this.http.post(`${this.url}/abonos`, abono);
  }

  // tslint:disable-next-line:typedef
  updateAbono(id: string, abono: Abono) {
    return this.http.put(`${this.url}/abonos/${id}`, abono);
  }

  // tslint:disable-next-line:typedef
  deleteAbono(id: string) {
    return this.http.delete(`${this.url}/abonos/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getAbonosSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getAbonoSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createAbonoSpring(abono: Abono){
    return this.http.post(`${this.url1}/create`, abono);
  }

  // tslint:disable-next-line:typedef
  updateAbonosSpring(updateabono: Abono): Observable<Abono>{
    return this.http.put(`${this.url1}/update`, updateabono);
  }

  // tslint:disable-next-line:typedef
  deleteAbonosSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
