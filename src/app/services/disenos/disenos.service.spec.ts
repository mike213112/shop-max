import { TestBed } from '@angular/core/testing';

import { DisenosService } from './disenos.service';

describe('DisenosService', () => {
  let service: DisenosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DisenosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
