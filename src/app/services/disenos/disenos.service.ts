import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Disenos } from '../../models/disenos/disenos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DisenosService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/disenos';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getDisenos() {
    return this.http.get(`${this.url}/disenos`);
  }

  // tslint:disable-next-line:typedef
  getDiseno(id: string) {
    return this.http.get(`${this.url}/disenos/${id}`);
  }

  // tslint:disable-next-line:typedef
  createDiseno(disenos: Disenos) {
    return this.http.post(`${this.url}/disenos`, disenos);
  }

  // tslint:disable-next-line:typedef
  updateDiseno(id: string, disenos: Disenos) {
    return this.http.put(`${this.url}/disenos/${id}`, disenos);
  }

  // tslint:disable-next-line:typedef
  deleteDiseno(id: string) {
    return this.http.delete(`${this.url}/disenos/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getDisenosSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getDisenoSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createDisenoSpring(disenos: Disenos){
    return this.http.post(`${this.url1}/create`, disenos);
  }

  // tslint:disable-next-line:typedef
  updateDisenoSpring(updatedisenos: Disenos): Observable<Disenos>{
    return this.http.put(`${this.url1}/update`, updatedisenos);
  }

  // tslint:disable-next-line:typedef
  deleteDisenoSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}

