import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Flores } from '../../models/flores/flores';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FloresService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/flores';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getFlores() {
    return this.http.get(`${this.url}/flores`);
  }

  // tslint:disable-next-line:typedef
  getFlore(id: string) {
    return this.http.get(`${this.url}/flores/${id}`);
  }

  // tslint:disable-next-line:typedef
  createFlore(flores: Flores) {
    return this.http.post(`${this.url}/flores`, flores);
  }

  // tslint:disable-next-line:typedef
  updateFlore(id: string, flores: Flores) {
    return this.http.put(`${this.url}/flores/${id}`, flores);
  }

  // tslint:disable-next-line:typedef
  deleteFlore(id: string) {
    return this.http.delete(`${this.url}/flores/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getFloresSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getFloreSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createFloreSpring(flores: Flores){
    return this.http.post(`${this.url1}/create`, flores);
  }

  // tslint:disable-next-line:typedef
  updateFloreSpring(updateflores: Flores): Observable<Flores>{
    return this.http.put(`${this.url1}/update`, updateflores);
  }

  // tslint:disable-next-line:typedef
  deleteFloreSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
