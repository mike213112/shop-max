import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Product } from '../../models/producto/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = 'http://localhost:3000/api';
  url2 = 'http://localhost:2020/product';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line: typedef
  getProducts(){
    return this.http.get(`${this.url}/product`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getProduct(id: string){
    return this.http.get(`${this.url}/product/${id}`);
  }

  // tslint:disable-next-line:typedef
  createProduct(product: Product){
    return this.http.post(`${this.url}/product`, product);
  }

  // tslint:disable-next-line:typedef
  updateProduct(id: string|number, updateProduct: Product): Observable<Product>{
    return this.http.put(`${this.url}/update`, updateProduct);
  }

  // tslint:disable-next-line:typedef
  deleteProduct(id: string){
    return this.http.delete(`${this.url}/product/${id}`);
  }

  // tslint:disable-next-line:comment-format
  //####################################################################################################

  // tslint:disable-next-line:comment-format
  // tslint:disable-next-line:typedef
  getProductswithspring(){
    return this.http.get(`${this.url2}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getProductwithspring(id: string){
    return this.http.get(`${this.url2}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createProductwithspring(product: Product){
    return this.http.post(`${this.url2}/create`, product);
  }

  // tslint:disable-next-line:typedef
  updateProductwithspring(updateProduct: Product): Observable<Product>{
    return this.http.put(`${this.url2}/update`, updateProduct);
  }

  // tslint:disable-next-line:typedef
  deleteProductwithspring(id: string){
    return this.http.delete(`${this.url2}/${id}`);
  }

}
