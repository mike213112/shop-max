import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Herramienta } from '../../models/herramienta/herramienta';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HerramientaService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/herramientas';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getHerramientas() {
    return this.http.get(`${this.url}/herramientas`);
  }

  // tslint:disable-next-line:typedef
  getHerramienta(id: string) {
    return this.http.get(`${this.url}/herramientas/${id}`);
  }

  // tslint:disable-next-line:typedef
  createHerramienta(herramienta: Herramienta) {
    return this.http.post(`${this.url}/herramientas`, herramienta);
  }

  // tslint:disable-next-line:typedef
  updateHerramienta(id: string, herramienta: Herramienta) {
    return this.http.put(`${this.url}/herramientas/${id}`, herramienta);
  }

  // tslint:disable-next-line:typedef
  deleteHerramienta(id: string) {
    return this.http.delete(`${this.url}/herramienta/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getHerramientasSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getHerramientaSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createHerramientaSpring(herramienta: Herramienta){
    return this.http.post(`${this.url1}/create`, herramienta);
  }

  // tslint:disable-next-line:typedef
  updateHerramientaSpring(updateherramienta: Herramienta): Observable<Herramienta>{
    return this.http.put(`${this.url1}/update`, updateherramienta);
  }

  // tslint:disable-next-line:typedef
  deleteHerramientaSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
