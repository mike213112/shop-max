import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cesped } from '../../models/cesped/cesped';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CespedService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/cesped';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getCespedes() {
    return this.http.get(`${this.url}/cesped`);
  }

  // tslint:disable-next-line:typedef
  getCesped(id: string) {
    return this.http.get(`${this.url}/cesped/${id}`);
  }

  // tslint:disable-next-line:typedef
  createCesped(cesped: Cesped) {
    return this.http.post(`${this.url}/cesped`, cesped);
  }

  // tslint:disable-next-line:typedef
  updateCesped(id: string, cesped: Cesped) {
    return this.http.put(`${this.url}/cesped/${id}`, cesped);
  }

  // tslint:disable-next-line:typedef
  deleteCesped(id: string) {
    return this.http.delete(`${this.url}/cesped/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getCespedesSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getCespedSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createCespedSpring(cesped: Cesped){
    return this.http.post(`${this.url1}/create`, cesped);
  }

  // tslint:disable-next-line:typedef
  updateCespedSpring(updatecesped: Cesped): Observable<Cesped>{
    return this.http.put(`${this.url1}/update`, updatecesped);
  }

  // tslint:disable-next-line:typedef
  deleteCespedSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
