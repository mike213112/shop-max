import { TestBed } from '@angular/core/testing';

import { CespedService } from './cesped.service';

describe('CespedService', () => {
  let service: CespedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CespedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
