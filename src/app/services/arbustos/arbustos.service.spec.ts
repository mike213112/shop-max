import { TestBed } from '@angular/core/testing';

import { ArbustosService } from './arbustos.service';

describe('ArbustosService', () => {
  let service: ArbustosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArbustosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
