import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Arbustos } from '../../models/arbustos/arbustos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArbustosService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/arbustos';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getArbustos() {
    return this.http.get(`${this.url}/arbustos`);
  }

  // tslint:disable-next-line:typedef
  getArbusto(id: string) {
    return this.http.get(`${this.url}/arbustos/${id}`);
  }

  // tslint:disable-next-line:typedef
  createArbustos(arbustos: Arbustos) {
    return this.http.post(`${this.url}/arbustos`, arbustos);
  }

  // tslint:disable-next-line:typedef
  updateArbustos(id: string, arbustos: Arbustos) {
    return this.http.put(`${this.url}/arbustos/${id}`, arbustos);
  }

  // tslint:disable-next-line:typedef
  deleteArbustos(id: string) {
    return this.http.delete(`${this.url}/arbustos/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getArbustosSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getArbustoSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createArbustosSpring(arbustos: Arbustos){
    return this.http.post(`${this.url1}/create`, arbustos);
  }

  // tslint:disable-next-line:typedef
  updateArbustosSpring(updatearbustos: Arbustos): Observable<Arbustos>{
    return this.http.put(`${this.url1}/update`, updatearbustos);
  }

  // tslint:disable-next-line:typedef
  deleteArbustosSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
