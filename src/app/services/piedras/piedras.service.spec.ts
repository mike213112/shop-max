import { TestBed } from '@angular/core/testing';

import { PiedrasService } from './piedras.service';

describe('PiedrasService', () => {
  let service: PiedrasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PiedrasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
