import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Piedras } from '../../models/piedras/piedras';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PiedrasService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/piedras';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getPiedras() {
    return this.http.get(`${this.url}/piedras`);
  }

  // tslint:disable-next-line:typedef
  getPiedra(id: string) {
    return this.http.get(`${this.url}/piedras/${id}`);
  }

  // tslint:disable-next-line:typedef
  createPiedra(piedras: Piedras) {
    return this.http.post(`${this.url}/piedras`, piedras);
  }

  // tslint:disable-next-line:typedef
  updatePiedra(id: string, piedras: Piedras) {
    return this.http.put(`${this.url}/piedras/${id}`, piedras);
  }

  // tslint:disable-next-line:typedef
  deletePiedra(id: string) {
    return this.http.delete(`${this.url}/piedras/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getPiedrasSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getPiedraSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createPiedrasSpring(piedras: Piedras){
    return this.http.post(`${this.url1}/create`, piedras);
  }

  // tslint:disable-next-line:typedef
  updatePiedrasSpring(updatepiedras: Piedras): Observable<Piedras>{
    return this.http.put(`${this.url1}/update`, updatepiedras);
  }

  // tslint:disable-next-line:typedef
  deletePiedrasSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
