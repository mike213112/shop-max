import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Arbol } from '../../models/arbol/arbol';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArbolService {

  url = 'http://localhost:3000/api';
  url1 = 'http://localhost:2020/arboles';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line: typedef
  getArboles() {
    return this.http.get(`${this.url}/arboles`);
  }

  // tslint:disable-next-line:typedef
  getArbol(id: string) {
    return this.http.get(`${this.url}/arboles/${id}`);
  }

  // tslint:disable-next-line:typedef
  createArbol(arboles: Arbol) {
    return this.http.post(`${this.url}/arboles`, arboles);
  }

  // tslint:disable-next-line:typedef
  updateArbol(id: string, arboles: Arbol) {
    return this.http.put(`${this.url}/arboles/${id}`, arboles);
  }

  // tslint:disable-next-line:typedef
  deleteArbol(id: string) {
    return this.http.delete(`${this.url}/arboles/${id}`);
  }

  // #########################################################################################################

  // tslint:disable-next-line:typedef
  getArbolesSpring(){
    return this.http.get(`${this.url1}/all`);
  }

  // tslint:disable-next-line:comment-format
  //tslint:disable-next-line:typedef
  getArbolSpring(id: string){
    return this.http.get(`${this.url1}/${id}`);
  }

  // tslint:disable-next-line:typedef
  createArbolSpring(arboles: Arbol){
    return this.http.post(`${this.url1}/create`, arboles);
  }

  // tslint:disable-next-line:typedef
  updateArbolSpring(updatearbol: Arbol): Observable<Arbol>{
    return this.http.put(`${this.url1}/update`, updatearbol);
  }

  // tslint:disable-next-line:typedef
  deleteArbolSpring(id: string){
    return this.http.delete(`${this.url1}/${id}`);
  }

}
